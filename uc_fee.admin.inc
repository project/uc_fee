<?php

/**
 * @file
 * fee administration menu items.
 *
 */

// Builds a paged list and overview of existing product fees.
function uc_fee_admin_form() {
  
  $header = _uc_fee_admin_header();
  $form = array();

  $result = pager_query("SELECT f.fid, f.name, f.price, f.ordering FROM {uc_fees} AS f GROUP BY f.fid, f.name, f.ordering, f.price". tablesort_sql($header), 30, 0, "SELECT COUNT(fid) FROM {uc_fees}");
  while ($attr = db_fetch_object($result)) {
    $form['fees'][$attr->fid] = array(
      'name' => array(
        '#value' => $attr->name,
      ),
      'ordering' => array(
        '#type' => 'weight',
        '#default_value' => $attr->ordering,
        '#fees' => array('class' => 'uc-fee-table-weight'),
      ),
      'price' => array(
        '#value' => $attr->price,
      ),
      'ops' => array(
        '#value' => l(t('edit'), 'admin/store/fees/'. $attr->fid .'/edit') .' '.
                    l(t('delete'), 'admin/store/fees/'. $attr->fid .'/delete') .' ',
      ),
    );
  }

  if (!empty($form['fees'])) {
    $form['fees']['#tree'] = TRUE;

    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save changes'),
      '#weight' => 10,
    );
  }

  return $form;
}

// Displays a paged list and overview of existing product fees.
function theme_uc_fee_admin_form($form) {
  $header = _uc_fee_admin_header();

  if (count(element_children($form['fees'])) > 0) {
    foreach (element_children($form['fees']) as $fid) {
      $row = array(
        drupal_render($form['fees'][$fid]['name']),
        drupal_render($form['fees'][$fid]['ordering']),
        drupal_render($form['fees'][$fid]['price']),
        drupal_render($form['fees'][$fid]['ops']),
      );

      $rows[] = array(
        'data' => $row,
        'class' => 'draggable',
      );
    }
  }
  else {
    $rows[] = array(
      array('data' => t('No fees have been added yet.'), 'colspan' => 6),
    );
  }

  drupal_add_tabledrag('uc-fee-table', 'order', 'sibling', 'uc-fee-table-weight');

  $output = theme('table', $header, $rows, array('id' => 'uc-fee-table'));

  $output .= drupal_render($form);

  $output .= l(t('Add a fee'), 'admin/store/fees/add');

  return $output;
}

function uc_fee_admin_form_submit($form, &$form_state) {

  foreach ($form_state['values']['fees'] as $fid => $fee) {
    db_query("UPDATE {uc_fees} SET ordering = %d WHERE fid = %d", $fee['ordering'], $fid);
  }

  drupal_set_message(t('The changes have been saved.'));
}

function _uc_fee_admin_header() {

  return array(
    array('data' => t('Name'), 'field' => 'f.name'),
    array('data' => t('Order'), 'field' => 'f.ordering', 'sort' => 'asc'),
    array('data' => t('Price'), 'field' => 'f.price'),
    t('Operations'),
  );

}


/**
 * Form builder for product fees.
 *
 * @ingroup forms
 * @see uc_fee_form_validate()
 * @see uc_fee_form_submit()
 */
function uc_fee_form($form_state, $fee = NULL) {
  // If an fee specified, add its ID as a hidden value.

  if (!empty($fee)) {
    $form['fid'] = array('#type' => 'hidden', '#value' => $fee->fid);
    drupal_set_title(t('Edit fee: %name', array('%name' => $fee->name)));
  }

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('This name will appear to customers on product add to cart forms.'),
    '#default_value' => $fee->name,
    '#required' => TRUE,
  );
  $form['description'] = array(
    '#type' => 'textfield',
    '#title' => t('Description'),
    '#description' => t('<b>Optional.</b> Enter the description that will display beneath the fee in the checkout and order panes.'),
    '#default_value' => $fee->description,
    '#maxlength' => 255,
  );
  $form['price'] = array(
    '#type' => 'textfield',
    '#title' => t('Price'),
    '#default_value' => is_null($fee->price) ? $fee->default_price : $fee->price,
    '#size' => 6,
  );
  $form['ordering'] = array(
    '#type' => 'weight',
    '#title' => t('Order'),
    '#description' => t('Multiple fees are sorted by this value and then by their name.<br />May be overridden at the product level.'),
    '#default_value' => isset($fee->ordering) ? $fee->ordering : 0,
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
    '#suffix' => l(t('Cancel'), 'admin/store/fees'),
  );

  return $form;
}

/**
 * Submit function for uc_fee_add_form().
 */
function uc_fee_form_submit($form, &$form_state) {
  if (!empty($form_state['values']['fid'])) {
    db_query("UPDATE {uc_fees} SET name = '%s', ordering = %d, description = '%s', price = %d WHERE fid = %d", $form_state['values']['name'], $form_state['values']['ordering'], $form_state['values']['description'], $form_state['values']['price'], $form_state['values']['fid']);
  }
  else {
    db_query("INSERT INTO {uc_fees} (name, ordering, description, price) VALUES ('%s', %d, '%s', %d)", $form_state['values']['name'], $form_state['values']['ordering'], $form_state['values']['description'], $form_state['values']['price']);
  }

  $form_state['redirect'] = 'admin/store/fees';
}

// Confirms the deletion of the given fee.
function uc_fee_delete_confirm($form_state, $fee) {
  // If we got a bunk fee, kick out an error message.
  if (empty($fee)) {
    drupal_set_message(t('There is no fee with that ID.'), 'error');
    drupal_goto('admin/store/fees');
  }

  $form['fid'] = array('#type' => 'value', '#value' => $fee->fid);
  $form['#redirect'] = 'admin/store/fees';

  $count = db_result(db_query("SELECT COUNT(*) FROM {uc_product_fees} WHERE fid = %d", $fee->fid));

  $output = confirm_form($form, t('Are you sure you want to delete the fee %name?', array('%name' => $fee->name)),
              'admin/store/fees', format_plural($count, 'There is @count product with this fee.', 'There are @count products with this fee.'),
              t('Delete'), t('Cancel'));

  return $output;
}

/**
 * Change the display of fee option prices.
 *
 * @ingroup forms
 */
function uc_fee_admin_settings() {
  $form = array();

  $form['uc_fee_option_price_format'] = array('#type' => 'radios',
    '#title' => t('Option price format'),
    '#default_value' => variable_get('uc_fee_option_price_format', 'adjustment'),
    '#options' => array('none' => t('Do not display'),
      'adjustment' => t('Display price adjustment'),
      'total' => t('Display total price'),
    ),
    '#description' => t('Formats the price in the fee selection form when the customer adds a product to their cart. The total price will only be displayed on products with only one price affecting fee.'),
  );

  return system_settings_form($form);
}

function uc_fee_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    db_query("DELETE FROM {uc_class_fees} WHERE fid = %d", $form_state['values']['fid']);
    db_query("DELETE FROM {uc_product_fees} WHERE fid = %d", $form_state['values']['fid']);
    db_query("DELETE FROM {uc_fees} WHERE fid = %d", $form_state['values']['fid']);
    drupal_set_message(t('Product fee deleted.'));
  }
}

// Form to associate fees with products or classes.
function uc_object_fees_form($form_state, $object, $type, $view = 'overview') {
  switch ($type) {
    case 'class':
      $class = $object;
      $id = $class->pcid;
      if (empty($class->name)) {
        drupal_goto('admin/store/products/classes/'. $id);
      }
      drupal_set_title(check_plain($class->name));
      $fees = uc_class_get_fees($id);
      break;
    case 'product':
    default:
      $product = $object;
      $id = $product->nid;
      if (empty($product->title)) {
        drupal_goto('node/'. $id);
      }
      drupal_set_title(check_plain($product->title));
      $fees = uc_product_get_fees($id);
  }

  $used_fids = array();
  foreach ($fees as $fee) {
    $used_fids[] = $fee->fid;
  }

  if ($view == 'overview') {
    $form['#tree'] = TRUE;
    if (count($fees) > 0) {
      foreach ($fees as $fee) {

        $form['fees'][$fee->fid] = array(
          'remove' => array(
            '#type' => 'checkbox',
            '#default_value' => 0,
          ),
          'name' => array(
            '#value' => $fee->name,
          ),
          'ordering' => array(
            '#type' => 'weight',
            '#default_value' => $fee->ordering,
            '#fees' => array('class' => 'uc-fee-table-weight'),
          ),
          'price' => array(
            '#type' => 'textfield',
            '#default_value' => is_null($fee->price) ? $fee->default_price : $fee->price,
            '#size' => 6,
          ),
        );
      }

      $form['save'] = array(
        '#type' => 'submit',
        '#value' => t('Save changes'),
        '#weight' => -2,
      );
    }
  }
  elseif ($view == 'add') {
    // Get list of fees not already assigned to this node or class.
    $unused_fees = array();
    $result = db_query("SELECT f.fid, f.name FROM {uc_fees} AS f GROUP BY f.fid, f.name ORDER BY f.name");
    while ($fee = db_fetch_object($result)) {
      if (!in_array($fee->fid, $used_fids)) {
        $unused_fees[$fee->fid] = $fee->name;
      }
    }

    $form['add_fees'] = array(
      '#type' => 'select',
      '#title' => t('fees'),
      '#description' => t('Hold Ctrl + click to select multiple fees.'),
      '#options' => count($unused_fees) > 0 ? $unused_fees : array(t('No fees left to add.')),
      '#disabled' => count($unused_fees) == 0 ? TRUE : FALSE,
      '#multiple' => TRUE,
      '#weight' => -1
    );
    $form['add'] = array(
      '#type' => 'submit',
      '#value' => t('Add fees'),
      '#suffix' => l(t('Cancel'), $type == 'product' ? 'node/'. $id .'/edit/fees' : 'admin/store/products/classes/'. $class->pcid .'/fees'),
      '#weight' => 0,
    );
  }

  $form['id'] = array(
    '#type' => 'value',
    '#value' => $id,
  );
  $form['type'] = array(
    '#type' => 'value',
    '#value' => $type,
  );
  $form['view'] = array(
    '#type' => 'value',
    '#value' => $view,
  );

  return $form;
}

/**
 * Display the formatted fee form.
 *
 * @ingroup themeable
 */
function theme_uc_object_fees_form($form) {
  if ($form['view']['#value'] == 'overview') {
    $header = array(t('Remove'), t('Name'), t('Price'), t('Order'));

    if (count(element_children($form['fees'])) > 0) {
      foreach (element_children($form['fees']) as $fid) {
        $row = array(
        
          drupal_render($form['fees'][$fid]['remove']),
          drupal_render($form['fees'][$fid]['name']),
          drupal_render($form['fees'][$fid]['price']),
          drupal_render($form['fees'][$fid]['ordering']),
        );

        $rows[] = array(
          'data' => $row,
          'class' => 'draggable',
        );
      }
    }
    else {
      $rows[] = array(
        array('data' => t('You must first <a href="!url">add fees to this !type</a>.', array('!url' => request_uri() .'/add', '!type' => $form['type']['#value'])), 'colspan' => 6),
      );
    }

    drupal_add_tabledrag('uc-fee-table', 'order', 'sibling', 'uc-fee-table-weight');
    $output = theme('table', $header, $rows, array('id' => 'uc-fee-table'));
  }

  $output .= drupal_render($form);

  return $output;
}

function uc_object_fees_form_submit($form, &$form_state) {
  if ($form_state['values']['type'] == 'product') {
    $fee_table = '{uc_product_fees}';
    $id = 'nid';
    $sql_type = '%d';
  }
  elseif ($form_state['values']['type'] == 'class') {
    $fee_table = '{uc_class_fees}';
    $id = 'pcid';
    $sql_type = "'%s'";
  }

  if ($form_state['values']['view'] == 'overview' && is_array($form_state['values']['fees'])) {
    foreach ($form_state['values']['fees'] as $fid => $fee) {
      if ($fee['remove']) {
        $remove_fids[] = $fid;
      }
      else {
        db_query("UPDATE $fee_table SET ordering = %d, default_price = %f WHERE fid = %d AND $id = $sql_type", $form_state['values']['fees'][$fid]['ordering'], $form_state['values']['fees'][$fid]['price'], $fid, $form_state['values']['id']);
        $changed = TRUE;
      }
    }

    if (count($remove_fids) > 0) {
      $values = array($form_state['values']['id'], implode(', ', $remove_fids));

      db_query("DELETE FROM $fee_table WHERE $id = $sql_type AND fid IN (%s)", $values);

      drupal_set_message(format_plural(count($remove_fids), '@count fee has been removed.', '@count fees have been removed.'));
    }

    if ($changed) {
      drupal_set_message(t('The changes have been saved.'));
    }
  }
  elseif ($form_state['values']['view'] == 'add') {
    foreach ($form_state['values']['add_fees'] as $fid) {
      // Enable all options for added fees.
      $fee = uc_fee_load($fid);
      db_query("INSERT INTO $fee_table ($id, fid, ordering, default_price) VALUES ($sql_type, %d, %d, %f)", $form_state['values']['id'], $fee->fid, $fee->ordering, $fee->price);
    }
    if (count($form_state['values']['add_fees']) > 0) {
      drupal_set_message(format_plural(count($form_state['values']['add_fees']), '@count fee has been added.', '@count fees have been added.'));
    }
  }

  if ($form_state['values']['type'] == 'product') {
    $form_state['redirect'] = 'node/'. $form_state['values']['id'] .'/edit/fees';
  }
  else {
    $form_state['redirect'] = 'admin/store/products/classes/'. $form_state['values']['id'] .'/fees';
  }
}
